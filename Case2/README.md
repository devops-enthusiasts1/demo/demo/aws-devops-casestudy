# Task 2
1. Create a VPC with a public and private subnet
2. Launch an EC2 instance, inside the private subnet of VPC, and install apache through bootstrapping
3. Create a load balancer in the public subnet
4. Add the EC2 instance, under the load balancer

Terraform commands:
terraform init
terraform plan -out=plan.out
terrform apply "plan.out"
AWS Access and Secret Key have been added in CI/CD Varibles for security reasons.