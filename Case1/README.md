# Task 1: Architecture design

Aim - An Architectural design solution to run a highly secure and available web application that is backed
with microservices and datastores. This architecture will be designed considering AWS resources with the
proper security measures.

Services used:
1. Webserver application to serve website  ->  ELB, ASG, Launch Templates, EC2 Instances
2. 3 microservices application running on Spring Boot, named as - MSA1, MSA2, MSA3 -> EKS
3. SQL Database -> RDS MySQL
4. NO-SQL Database -> AWS DynamoDB
5. Memory Caching Datastore -> AWS ElastiCache
6. File Storage -> EFS
7. Logging -> AWS CloudWatch Logroups
8. Monitoring -> Amazon Managed Prometheus, AWS CloudWatch

Security Services:
1. AWS WAF
2. AWS Shield
3. AWS IAM
4. AWS KMS

